

## Sarah Huerta,

#### I am pursuing a B.S. in Information Technology with a focus on learning SQL and Database Management Skills. I am currently located in the Florida Panhandle, but am willing to travel across the state for work.

#### As a senior at Florida State University, I am pursuing a degree in Information Technology with an elective focus in Database Management and Architecture.

*Below you will find current and past projects I have developed during my time at Florida State.*

1. [LIS4369: Extensible Enterprise Solutions](lis4369/README.md)
    * Course Description: Employing skills to work within proprietary and nonproprietary Web application frameworks allow developers to build dynamic web sites and web applications in conjunction with data repositories and XML web services. This course will utilize industry-standard tools and techniques to create enterprise- class web applications.
    * Course Objectives
      * Utilize algorithms studied to perform common tasks, such as finding the max and min of a data set, counting, summing, tracking a previous value, searching and sorting, reading until EOF, etc;
      * Design and employ databases to build professional quality, data-driven applications, using proprietary and open source technologies and Employ user interfaces to read from and write to databases;


2. [LIS4381: Advanced Mobile Web Applications](lis4381/README.md)
    * Course Description: This course focuses on concepts and best practices for managing mobile technology projects. It covers processes and requirements for developing mobile applications and principles for effective interface and user experience design. Students develop a prototype of a mobile app and prepare a proposal and other documentation for communicating contractual and functional specifications to clients. Students also examine different issues and concerns that may influence the wide-spread adoption and implementation of mobile applications.
    * Course Objectives
      * Develop “mobile first” applications using HTML, CSS, JavaScript, jQuery, XML, Java skills, client- as well as server-side skills, and data repository interfaces;
      * Use industry standard design architectures when creating mobile applications, Utilize current development tools and APIs to build mobile applications, and Review and analyze mobile hardware and software limitations based upon current literature



  3. Interactive Resume
    * Details Current/Past Work Experience
    * Portfolio of Senior Design Project Management, Junior Research Paper, and other small projects.
    * [Click Here to be redirected.](https://sah16m.wixsite.com/sarahhuerta)
