> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4369 - Extensible Enterprise Solutions (Python)

## Sarah Huerta

### LIS4369

*Course Work Links*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Bullet-list items
    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code/Atom
    * Create "a1_tip_calculator" application
    * Create "a1_tip_calculator" Jupyter Notebook
    * Provide screenshots of installations
    * Create Bitbucket repos
    * Create Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Screenshot of Python Payroll Calculator
    * Link to A2 .ipynb file: a2_payroll.ipynb
    * Bitbucket Link to skill sets 1-3
3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Paint Calculator
    * Link to .ipynb file
    * Skill Sets 4-6

4. [P1 README.md](p1/README.md "My P1 README.md file")
    *  Run demo.py
    *  If errors, more than likely missing installations.
    *  Test Python Package installer: pip freeze
    *  Research how to do the following installations:
         *  pandas (only if missing)
         *  pandas-datareader (only if missing)
         *  matplotlib (only if missing)
    *  Create at least three functions that are called by the program:
        *  main(): calls at least two other functions.
        *  get_requirements(): displays the program requirements.
        *  data_analysis_1(): displays the following data.

5. [A4 README.md](a4/README.md "My A4 README.md file")
    *  Run demo.py
    *  If errors, more than likely missing installations.
    *  Test Python Package installer: pip freeze
    *  Create at least three functions that are called by the program:
        *  main(): calls at least two other functions.
        *  get_requirements(): displays the program requirements.
        *  data_analysis_2(): displays the following data.

5. [A5 README.md](a5/README.md "My A5 README.md file")
    *  Set up and install R Studio
    *  Run lis4369_a5.R and learn_to_use_r.R
    *  Display graphs and output
    *  Skill sets 13-15
